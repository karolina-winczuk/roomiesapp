import { IAction } from "../../Interfaces/IAction";
import { IExpencesState } from "../../Interfaces/IStoreState";
import { IExpence } from "../../Models/IExpence";
import { getDataAfterAddition, getInitialState } from "./ReduxHelpers";
import * as actionTypes from '../actions/actionTypes';

export const reducer = (state: IExpencesState = getInitialState(), action: IAction<IExpence | Array<IExpence> | string>) => {
	switch (action.type) {
		case actionTypes.ADD_EXPENCE:
			return { ...state, data: getDataAfterAddition(state, action.payload as IExpence), loading: false };
		case actionTypes.GET_EXPENCES:
			return { ...state, data: action.payload as Array<IExpence>, loading: false };
		case actionTypes.ROOMATES_REQUEST_STARTS:
			return { ...state, loading: true, error: "" }
		case actionTypes.ROOMATES_REQUEST_FAILS:
			return { ...state, loading: false, error: action.payload as string }
		default:
			return state;
	}
}