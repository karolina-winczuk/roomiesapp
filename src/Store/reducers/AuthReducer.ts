import { AuthController } from "../../Controllers/AuthController";
import { IAction } from "../../Interfaces/IAction";
import { IAuthState } from "../../Interfaces/IStoreState";
import { AuthData } from "../../Models/AuthData";
import * as actionTypes from "../actions/actionTypes";

export const getInitialState = () => {
  const cachedAuthData = AuthController.getCachedData();
  return {
    isAuth: !!cachedAuthData,
    ...(cachedAuthData || new AuthData()),
    loading: false,
    error: ""
  }
}

export const reducer = (state = getInitialState(), action: IAction<AuthData | string>): IAuthState => {
  switch (action.type) {
    case actionTypes.AUTH_STARTS:
      return { ...state, loading: true, error: "" };
    case actionTypes.AUTH_FAILS:
      return { ...state, loading: false, error: action.payload as string };
    case actionTypes.SIGN_UP:
      return { ...state, loading: false, error: "", isAuth: true, ...action.payload as AuthData }
    case actionTypes.LOG_IN:
      return { ...state, loading: false, error: "", isAuth: true, ...action.payload as AuthData }
    case actionTypes.LOG_OUT:
      return { ...state, loading: false, error: "", isAuth: false, ...action.payload as AuthData }
    default:
      return state;
  }
}