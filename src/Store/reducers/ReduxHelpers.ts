import { AxiosError } from "axios";
import { Dispatch } from "redux";
import { IDataReducerState } from "../../Interfaces/IStoreState";
import { resources } from "../../Resources/Resources";

export const getInitialState = <T extends { id: string }>() => ({
	data: [] as Array<T>,
	loading: false,
	error: ""
})

export const getDataAfterAddition = <T extends { id: string }>(state: IDataReducerState<T>, item: T): Array<T> => {
	if (state.data) 
		return [...state.data.map(r => ({ ...r })), item];
	return state.data;
}

export const getDataAfterDeletion = <T extends { id: string }>(state: IDataReducerState<T>, item: T): Array<T> => {
	return state.data?.filter(r => r.id !== item.id);
}

export const getDataAfterUpdate = <T extends { id: string }>(state: IDataReducerState<T>, item: T): Array<T> => {
	if (state.data) {
		return [ ...state.data.map(r => {
			if (r.id === item.id) return { ...item }
				return r;
		})]
	}
	return state.data;
}

export const dispatchFailAction = <T>(dispatch: Dispatch, actionType: string) => (
	(error: AxiosError<T>) => { dispatch({ type: actionType, payload: `${resources.messages.error} ${error.message}` })}
)
  