import * as actionTypes from '../actions/actionTypes';
import { IRoomatesState } from '../../Interfaces/IStoreState';
import { Roomate } from '../../Models/Roomate';
import { IAction } from '../../Interfaces/IAction';
import { getDataAfterAddition, getDataAfterDeletion, getDataAfterUpdate, getInitialState } from './ReduxHelpers';

export const reducer = (state: IRoomatesState = getInitialState(), action: IAction<Roomate | Array<Roomate> | string>): IRoomatesState => {
  switch (action.type) {
    case actionTypes.ADD_ROOMATE:
      return { ...state, data: getDataAfterAddition(state, action.payload as Roomate), loading: false };
    case actionTypes.DELETE_ROOMATE:
      return { ...state, data: getDataAfterDeletion(state, action.payload as Roomate), loading: false };
    case actionTypes.GET_ROOMATES:
      return { ...state, data: action.payload as Array<Roomate>, loading: false };
    case actionTypes.UPDATE_ROOMATE:
      return { ...state, data: getDataAfterUpdate(state, action.payload as Roomate), loading: false };
    case actionTypes.ROOMATES_REQUEST_STARTS:
      return { ...state, loading: true, error: "" }
    case actionTypes.ROOMATES_REQUEST_FAILS:
      return { ...state, loading: false, error: action.payload as string }
    default:
      return state;
  }
}