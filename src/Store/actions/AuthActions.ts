import { AxiosError } from "axios";
import { AuthController } from "../../Controllers/AuthController";
import { AuthCredential } from "../../Models/AuthCredential";
import { resources } from "../../Resources/Resources";
import * as actionTypes from "./actionTypes";


const controller = new AuthController();

const dispatchErrorAction = (dispatch: any) => (
  (error: AxiosError) => {
    dispatch({ type: actionTypes.AUTH_FAILS, payload: error.response?.data?.error?.message || resources.messages.error })
    throw error;
  }
)

export const signUp = (payload: AuthCredential, resolve: () => void) => {
  return (dispatch: any) => {
    dispatch({ type: actionTypes.AUTH_STARTS })
    return controller.signUp(payload)
      .then(data => {
        resolve();
        return dispatch({
          type: actionTypes.SIGN_UP,
          payload: data
        })
      })
      .catch(dispatchErrorAction(dispatch))
  }
}

export const logIn = (payload: AuthCredential, resolve: () => void) => {
  return (dispatch: any) => {
    dispatch({ type: actionTypes.AUTH_STARTS })
    return controller.logIn(payload)
      .then(data => {
        resolve();
        return dispatch({
          type: actionTypes.SIGN_UP,
          payload: data
        })
      })
      .catch(dispatchErrorAction(dispatch))
  }
}

export const logOut = () => {
  return {
    type: actionTypes.LOG_OUT,
    payload: controller.logOut()
  }
}
