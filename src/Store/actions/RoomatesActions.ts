import { Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import { RoomatesController } from "../../Controllers/RoomatesController";
import { IAction } from "../../Interfaces/IAction";
import { IStoreState } from "../../Interfaces/IStoreState";
import { Roomate } from "../../Models/Roomate";
import { dispatchFailAction } from "../reducers/ReduxHelpers";
import * as actionTypes from "./actionTypes";

const controller = new RoomatesController();

export const addRoomate = (payload: Roomate) => {
  return (dispatch: Dispatch, getState: () => IStoreState) => {
    dispatch({ type: actionTypes.ROOMATES_REQUEST_STARTS });
    const authState = getState().auth;
    controller.create(payload, authState.tokenId, authState.houseId)
      .then(data => {
        dispatch({ 
          type: actionTypes.ADD_ROOMATE, 
          payload: data 
        })
      })
      .catch(dispatchFailAction(dispatch, actionTypes.ROOMATES_REQUEST_FAILS))  
  }
}

export const deleteRoomate = (payload: Roomate): ThunkAction<void, IStoreState, {}, IAction<Roomate>> => {
  return (dispatch: Dispatch, getState: () => IStoreState) => {
    dispatch({ type: actionTypes.ROOMATES_REQUEST_STARTS });
    controller.delete(payload?.id, getState().auth.tokenId)
      .then(() => {
        dispatch({
          type: actionTypes.DELETE_ROOMATE,
          payload: payload
        })
      })
      .catch(dispatchFailAction(dispatch, actionTypes.ROOMATES_REQUEST_FAILS))  
  }
}

export const getRoomates = () => {
  return (dispatch: Dispatch, getState: () => IStoreState) => {
    const authState = getState().auth;
    dispatch({ type: actionTypes.ROOMATES_REQUEST_STARTS });
    controller.read(authState.tokenId, authState.houseId)
      .then(data => {
        dispatch({
          type: actionTypes.GET_ROOMATES, 
          payload: data
        })
      })
      .catch(dispatchFailAction(dispatch, actionTypes.ROOMATES_REQUEST_FAILS))  
  }
}

export const updateRoomate = (roomate: Roomate) => {
  return (dispatch: Dispatch<IAction<Roomate>>, getState: () => IStoreState) => {
	const authState = getState().auth;
    dispatch({ type: actionTypes.ROOMATES_REQUEST_STARTS });
    controller.update(roomate, authState.tokenId, authState.houseId)
      .then(data => {
        dispatch({
          type: actionTypes.UPDATE_ROOMATE, 
          payload: data
        })
      })
      .catch(dispatchFailAction(dispatch, actionTypes.ROOMATES_REQUEST_FAILS))  
  }
}