import { Dispatch } from "redux";
import { ExpencesController } from "../../Controllers/ExpencesController";
import { IStoreState } from "../../Interfaces/IStoreState";
import { IExpence } from "../../Models/IExpence";
import * as actionTypes from '../actions/actionTypes';
import { dispatchFailAction } from "../reducers/ReduxHelpers";

const controller = new ExpencesController();
  
export const addExpence = (payload: IExpence) => {
	return (dispatch: Dispatch, getState: () => IStoreState) => {
		dispatch({ type: actionTypes.EXPENCES_REQUEST_STARTS });
		const authState = getState().auth;
		controller.create(payload, authState.tokenId, authState.houseId)
		.then(data => {
			dispatch({ 
			type: actionTypes.ADD_EXPENCE, 
			payload: data 
			})
		})
		.catch(dispatchFailAction(dispatch, actionTypes.EXPENCES_REQUEST_FAILS))  
	}
}

export const getExpences = () => {
	return (dispatch: Dispatch, getState: () => IStoreState) => {
		const authState = getState().auth;
		dispatch({ type: actionTypes.EXPENCES_REQUEST_STARTS });
		controller.read(authState.tokenId, authState.houseId)
		  .then(data => {
			dispatch({
			  type: actionTypes.GET_EXPENCES, 
			  payload: data
			})
		  })
		  .catch(dispatchFailAction(dispatch, actionTypes.EXPENCES_REQUEST_FAILS))  
	  }
}