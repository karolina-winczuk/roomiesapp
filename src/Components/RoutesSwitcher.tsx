import React from "react";
import { Route, Switch } from "react-router-dom";
import RoomateForm from "./Forms/RoomateForm";
import AuthForm from "./Forms/AuthForm";
import HomeView from "./Views/Home/HomeView";
import RoomatesView from "./Views/Roomates/RoomatesView";
import LogOut from "./Auth/LogOut";
import { useSelector } from "react-redux";
import { IStoreState } from "../Interfaces/IStoreState";
import { ExpencesView } from "./Views/Expences/ExpencesView";
import ExpenceForm from "./Forms/ExpenceForm";

export const RoutesSwitcher = () => {
  const isAuth = useSelector<IStoreState, boolean>(state => state.auth.isAuth);
  let routes = isAuth ?
    (<React.Fragment>
      <Route exact path="/roomates" component={RoomatesView} />
      <Route path="/roomates/roomate-form" component={RoomateForm} />
      <Route exact path="/expences" component={ExpencesView} />
      <Route path="/expences/expence-form" component={ExpenceForm} />
      <Route path="/chores">
        <div>ChoresView</div>
      </Route>
      <Route path="/log-out" component={LogOut} />
      <Route exact path="/" component={HomeView} />
    </React.Fragment>) :
    (<React.Fragment>
      <Route path="/sign-up" render={() => <AuthForm type={"signup"} />} />
      <Route path="/log-in" render={() => <AuthForm type={"login"} />} />
      <Route exact path="/" component={HomeView} />
    </React.Fragment>);
  return (
    <Switch>
      {routes}
    </Switch>
  )
}