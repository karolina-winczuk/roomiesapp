import React from "react";
import { Link, useLocation } from "react-router-dom";
import { resources } from "../../../Resources/Resources";

const Logo: React.FC<{}> = () => {
    const location = useLocation();
    const className = location.pathname === "/" ? "navbar-logo disabled" : "navbar-logo";
    return (
        <div className={className}>
            <Link to="/">{resources.logo}</Link>
        </div>
    )
}

export default Logo;