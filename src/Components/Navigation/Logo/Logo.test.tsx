import { render, screen } from "@testing-library/react"
import { BrowserRouter } from "react-router-dom";
import Logo from "./Logo"

describe("Logo", () => {
    test("renders Logo component", () => {
        render(<Logo />, { wrapper: BrowserRouter });
        expect(screen.getByRole("link")).toHaveAttribute("href", "/");
    });
})