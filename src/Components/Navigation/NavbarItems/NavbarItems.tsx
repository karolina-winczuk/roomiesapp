import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { IStoreState } from "../../../Interfaces/IStoreState";
import { resources } from "../../../Resources/Resources";

export const NavbarItems: React.FC<{}> = () => {
    const isAuth = useSelector<IStoreState, boolean>(state => state.auth.isAuth);
    const authResources = resources.auth;
    const links = isAuth ? 
        <NavLink to="/log-out">{authResources.logout}</NavLink> : 
        <React.Fragment>
            <NavLink to="/log-in">{authResources.login}</NavLink>
            <NavLink to="/sign-up">{authResources.signup}</NavLink>
        </React.Fragment>
    return (
        <div className="navbar-items">
            {links}
        </div>
    )
}