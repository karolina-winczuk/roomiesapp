import { render, screen } from "@testing-library/react"
import * as redux from "react-redux"
import { BrowserRouter } from "react-router-dom"
import React from "react"
import { NavbarItems } from "./NavbarItems"

describe("NavbarItems component", () => {
    let useSelectorMock: jest.SpyInstance;
    beforeEach(() => {
        useSelectorMock = jest.spyOn(redux, "useSelector")    
    })
    afterEach(() => {
        jest.clearAllMocks();
    })

    test("renders logout link when authenticated", () => {
        useSelectorMock.mockReturnValue(true);
        render(<NavbarItems />, { wrapper: BrowserRouter });
        expect(screen.getByRole("link")).toHaveAttribute("href", "/log-out");
    })

    test("renders login link when not authenticated", () => {
        useSelectorMock.mockReturnValue(true);
        render(<NavbarItems />, { wrapper: BrowserRouter });
        expect(screen.getByRole("link")).toHaveAttribute("href", "/log-out");
    })
})