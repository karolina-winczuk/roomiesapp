import React, { useState } from "react";
import { Menu } from "./Menu/Menu";
import { Navbar } from "./Navbar/Navbar";

export const Navigation: React.FC<{}> = () => {  
    const [showMenu, setShowMenu] = useState(false);
    const toggleMenu = () => {
        setShowMenu(prevShowMenu => !prevShowMenu);
    }
    return (
        <React.Fragment>
            <Navbar toggleMenu={toggleMenu} menuOpened={showMenu}/>
            <Menu toggleMenu={toggleMenu} show={showMenu} />
        </React.Fragment>
    )
}