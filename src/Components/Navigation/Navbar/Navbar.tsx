import Logo from "../Logo/Logo";
import { MenuToggler } from "../MenuToggler/MenuToggler";
import { NavbarItems } from "../NavbarItems/NavbarItems";

interface INavbarProps { 
  toggleMenu: (event: React.MouseEvent<HTMLDivElement>) => void, 
  menuOpened: boolean 
}

export const Navbar: React.FC<INavbarProps> = ({ toggleMenu, menuOpened }) => (
  <div className="navbar">
    <MenuToggler onClick={toggleMenu} menuOpened={menuOpened}/>
    <Logo />
    <NavbarItems />
  </div>
);