import React from "react";
import ReactDOM from "react-dom";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { IStoreState } from "../../../Interfaces/IStoreState";
import { resources } from "../../../Resources/Resources";
import { Backdrop } from "../../UI/Backdrop/Backdrop";

export const Menu: React.FC<{ show: boolean, toggleMenu: () => void }> = ({ show, toggleMenu }) => {
  const isAuth = useSelector<IStoreState, boolean>(state => state.auth.isAuth);
  ;
  return (
    show && isAuth ? 
      <div className="menu">
        {ReactDOM.createPortal(<Backdrop onClick={toggleMenu} />, document.querySelector("#backdrop-container")!)}
        <ul>
          <li><NavLink to="/roomates" onClick={toggleMenu}>{resources.roomates.name}</NavLink></li>
          <li><NavLink to="/expences" onClick={toggleMenu}>{resources.expences.name}</NavLink></li>
          <li><NavLink to="/chores" onClick={toggleMenu}>{resources.chores.name}</NavLink></li>
        </ul>
      </div> :
      null
  )
}