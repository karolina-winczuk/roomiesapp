import { render, screen } from "@testing-library/react"
import { Menu } from "./Menu"
import * as redux from "react-redux"
import { BrowserRouter } from "react-router-dom"

describe("Menu component", () => {
    let useSelectorMock: jest.SpyInstance;
    beforeEach(() => {
        useSelectorMock = jest.spyOn(redux, "useSelector");
        const div = document.createElement("div");
        div.id = "backdrop-container";
    })
    afterEach(() => {
        jest.clearAllMocks();
    })

    test("renders when authenticated and should be visible", () => {
        useSelectorMock.mockReturnValue(true);
        render(<Menu toggleMenu={jest.fn} show={true}/>, { wrapper: BrowserRouter, container: document.body });
        expect(screen.getAllByRole("link")).toHaveLength(3);
    })

    test("not renders when not authenticated and should not be visible", () => {
        useSelectorMock.mockReturnValue(true);
        render(<Menu toggleMenu={jest.fn} show={false}/>, { wrapper: BrowserRouter, container: document.body });
        expect(screen.queryByRole("link")).toBeNull();
    })

    test("not renders when not auenticated", () => {
        useSelectorMock.mockReturnValue(false);
        render(<Menu toggleMenu={jest.fn} show={true}/>, { wrapper: BrowserRouter, container: document.body });
        expect(screen.queryByRole("link")).toBeNull();
    })

    test("not renders when not auenticated and should not be visible", () => {
        useSelectorMock.mockReturnValue(false);
        render(<Menu toggleMenu={jest.fn} show={false}/>, { wrapper: BrowserRouter, container: document.body });
        expect(screen.queryByRole("link")).toBeNull();
    })
})