import { render, screen } from "@testing-library/react"
import { MenuToggler } from "./MenuToggler"
import * as redux from "react-redux"
import { BrowserRouter } from "react-router-dom"
import React from "react"
import userEvent from "@testing-library/user-event"

describe("MenuToggler component", () => {
    let useSelectorMock: jest.SpyInstance;
    beforeEach(() => {
        useSelectorMock = jest.spyOn(redux, "useSelector")    
    })
    afterEach(() => {
        jest.clearAllMocks();
    })

    test("not renders when not authenticated", () => {
        useSelectorMock.mockReturnValue(false);
        const { container } = render(<MenuToggler menuOpened={true} onClick={jest.fn()} />, { wrapper: BrowserRouter });
        expect(container.firstChild).toBeNull();
    })

    test("renders when authenticated", () => {
        useSelectorMock.mockReturnValue(true);
        const { container } = render(<MenuToggler menuOpened={true} onClick={jest.fn()} />, { wrapper: BrowserRouter });
        expect(container.firstChild).toHaveClass("toggle-menu");
    })

    test("renders as opened styled", () => {
        useSelectorMock.mockReturnValue(true);
        const { container } = render(<MenuToggler menuOpened={true} onClick={jest.fn()} />, { wrapper: BrowserRouter });
        expect(container.querySelector(".opened")).not.toBeNull();
    })

    test("renders as closed styled", () => {
        useSelectorMock.mockReturnValue(true);
        const { container } = render(<MenuToggler menuOpened={false} onClick={jest.fn()} />, { wrapper: BrowserRouter });
        expect(container.querySelector(".opened")).toBeNull();
    })
})