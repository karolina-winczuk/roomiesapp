import React from "react";
import { useSelector } from "react-redux";
import { IStoreState } from "../../../Interfaces/IStoreState";

interface IMenuToggglerProps {
    menuOpened: boolean,
    onClick: (event: React.MouseEvent<HTMLDivElement>) => void
}

export const MenuToggler: React.FC<IMenuToggglerProps> = props => {
    const isAuth = useSelector<IStoreState, boolean>(state => state.auth.isAuth);
    if (!isAuth) return null;
    else {
        const className = props.menuOpened ? "toggle-menu-bar opened" : "toggle-menu-bar";
        return (
            <div className="toggle-menu" onClick={props.onClick}>
                <div className={className}></div>
                <div className={className}></div>
                <div className={className}></div>
            </div>
        )
    }
}