import { useDispatch, useSelector } from 'react-redux';
import React from 'react';
import Form, { IFormProps } from './Form';
import { logIn, signUp } from '../../Store/actions/AuthActions';
import { IStoreState } from '../../Interfaces/IStoreState';
import { AuthCredential } from '../../Models/AuthCredential';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { resources } from '../../Resources/Resources';
import Loader from '../UI/Loader/Loader';
import Error from "../UI/Error/Error";
import { InputFieldType } from '../../Models/InputFieldType';

interface IAuthFormProps {
  type: "login" | "signup",
}

const AuthForm: React.FC<IAuthFormProps & RouteComponentProps> = props => {
  const loading = useSelector<IStoreState, boolean>(state => state.auth.loading);
  const error = useSelector<IStoreState, string>(state => state.auth.error);
  const dispatch = useDispatch();
  const saveFormData = (formData: AuthCredential) => {
    const saveHandler = props.type === "login" ? logIn : signUp;
    new Promise<void>(resolve => dispatch(saveHandler(formData, resolve)))
      .then(() => redirectToHomeView());
  }
  
  const cancelForm = () => {
    redirectToHomeView();
  }

  const redirectToHomeView = () => {
    props.history.push("/");
  }

  const authResources = resources.auth;
  const formProps: IFormProps<AuthCredential> = {
    schema: {
      login: {
        type: InputFieldType.Email,
        label: authResources.form.login.label,
        placeholder: authResources.form.login.placeholder,
        required: true,
        value: "",
        valid: false
      },
      password: {
        type: InputFieldType.Password,
        label: authResources.form.password.label,
        placeholder: authResources.form.password.placeholder,
        required: true,
        value: "",
        valid: false
      }
    },
    saveFormData: saveFormData,
    cancelForm: cancelForm
  }
  const formName = props.type === "signup" ? authResources.signup : authResources.login; 
  return (
    <div className="authform">
      <h2>{formName}</h2>
      <div className="form-wrapper">
        <Form {...formProps}/>
      </div>
      <Loader show={loading} />
      <Error message={error} />
    </div>
  )
}

export default withRouter(AuthForm);