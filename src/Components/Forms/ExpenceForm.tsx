import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect } from 'react';
import Form, { IFormProps } from './Form';
import { Roomate } from '../../Models/Roomate';
import { resources } from '../../Resources/Resources';
import { InputFieldType } from '../../Models/InputFieldType';
import { SelectFieldType } from '../../Models/SelectFieldType';
import { IStoreState } from '../../Interfaces/IStoreState';
import { getRoomates } from '../../Store/actions/RoomatesActions';
import { IExpence } from '../../Models/IExpence';
import { useHistory } from 'react-router';
import SectionHeader from '../UI/SectionHeader/SectionHeader';
import { addExpence } from '../../Store/actions/ExpencesActions';
import { currencies } from '../../Models/Currency';

const ExpenceForm: React.FC<{}> = () => {
  const expencesResources = resources.expences;
  const dispatch = useDispatch();
  const roomates = useSelector<IStoreState, Array<Roomate>>(state => state.roomates.data);
  const history = useHistory();

  useEffect(() => {
    if (!roomates?.length) {
      dispatch(getRoomates());
    }
  }, [])

  const saveFormData = (formData: IExpence) => {
	dispatch(addExpence(formData));
    redirectToExpencesView();
  }

  const cancelForm = () => {
    redirectToExpencesView()
  }

  const redirectToExpencesView = () => {
    history.push("/expences");
  }

  const formProps: IFormProps<IExpence> = {
    schema: {
      amount: {
        type: InputFieldType.Number,
        label: expencesResources.forms.amount.label,
        placeholder: expencesResources.forms.amount.label,
        required: true,
        value: 0,
        valid: false
      },
	  currency: {
        type: SelectFieldType.Single,
        label: expencesResources.forms.currency.label,
        placeholder: "Select..",
        required: true,
        value: currencies?.map(c => ({ ...c, key: c.symbol, value: c.symbol }))[0],
        valid: false,
        options: currencies?.map(c => ({ ...c, key: c.symbol, value: c.symbol }))
      },
      paidBy: {
        type: SelectFieldType.Single,
        label: expencesResources.forms.paidBy.label,
        placeholder: "Select..",
        required: true,
        value: null,
        valid: false,
        options: roomates?.map(roomate => ({ ...roomate, key: roomate.id, value: roomate.name }))
      },
      paidFor: {
        type: SelectFieldType.Multi,
        label: expencesResources.forms.paidFor.label,
        required: true,
        value: [],
        valid: false,
        options: roomates?.map(roomate => ({ ...roomate, key: roomate.id, value: roomate.name }))
      }
    },
    saveFormData,
    cancelForm
  }
  return (
    <div className="section">
      <SectionHeader title={expencesResources.addExpence} loading={false}/>
      <div className="section-content form">
        <Form {...formProps}/>
      </div>
    </div>
  )
}

export default ExpenceForm;