import React, { useEffect, useState } from 'react';
import { IFormFieldSchema } from '../../Interfaces/IFormFieldSchema';
import FormField from '../UI/FormField/FormField';
import { resources } from '../../Resources/Resources';
import { FormFieldSchema } from '../../Models/FormFieldSchema';
import { ISelectOption } from '../../Models/ISelectOption';

export interface IFormProps<T> {
  schema: { [fieldName: string]: FormFieldSchema };
  saveFormData: (formData: T) => void;
  cancelForm: () => void;
}

function Form<T>(props: IFormProps<T>) {
  const [schema, setFormSchema] = useState(props.schema);
  const [isValid, setIsValid] = useState(false);

  const checkFieldValidity = (fieldSchema: IFormFieldSchema) => {
    // if (fieldSchema.required) {
    //   return fieldSchema.value.trim().length > 0;
    // }
    return true;
  }

  useEffect(() => {
    setIsValid(checkOverallValidity(schema));
  }, [schema]);

  useEffect(() => {
    setFormSchema(props.schema);
  }, [props.schema])

  const checkOverallValidity = (formSchema: { [fieldName: string]: IFormFieldSchema}) => {
    let isFormValid = true;
    Object.keys(formSchema).forEach((fieldName: string) => {
      isFormValid = isFormValid && formSchema[fieldName].valid;
    });
    return isFormValid;
  }

  const handleChange = (value: string | number | ISelectOption | Array<ISelectOption>, fieldName: string) => {
    setFormSchema((prevSchema) => {
      const newFieldSchema = {
        ...prevSchema[fieldName],
        value: value
      }
      return {
        ...prevSchema,
        [fieldName]: {
          ...newFieldSchema,
          valid: checkFieldValidity(newFieldSchema)
        }
      }
    })
  }

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formDataObject = {} as T;
    Object.keys(schema).forEach((fieldName: string) => { 
      formDataObject[fieldName as keyof T] = schema[fieldName].value 
    })
    props.saveFormData(formDataObject);
  }
  
  const handleCancel = () => {
    props.cancelForm();
  }

  const formFields = Object.keys(schema).map((fieldName: string) => (
    <div className="form-field" key={fieldName}>
      <FormField 
        fieldSchema={schema[fieldName]}  
        fieldName={fieldName}
        handleChange={handleChange} />
    </div>
  ))
  const buttonNames = resources.buttons;
  return (
    <form onSubmit={handleSubmit}>
      {formFields}
      <div className="form-output">
        <button className="submit-button" type="submit" disabled={!isValid}>{buttonNames.submit}</button>
        <button className="submit-button" onClick={handleCancel}>{buttonNames.cancel}</button>
      </div>
    </form>
  )
}

export default Form;