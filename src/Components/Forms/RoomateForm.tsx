import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import React from 'react';
import Form, { IFormProps } from './Form';
import { Roomate } from '../../Models/Roomate';
import { addRoomate, updateRoomate } from '../../Store/actions/RoomatesActions';
import { IStoreState } from '../../Interfaces/IStoreState';
import { resources } from '../../Resources/Resources';
import { InputFieldType } from '../../Models/InputFieldType';
import SectionHeader from '../UI/SectionHeader/SectionHeader';

const RoomateForm: React.FC<RouteComponentProps> = props => {
  const roomates = useSelector<IStoreState, Array<Roomate>>(state => state.roomates.data);
  const dispatch = useDispatch();
  const roomatesResources = resources.roomates;
  const roomateId = new URLSearchParams(props.location.search).get('id');
  const roomate = roomateId ? roomates?.filter((r: Roomate) => r.id === roomateId)[0] : null;
  const formName = roomateId ? roomatesResources.editRoomate : roomatesResources.addRoomate;

  const saveFormData = (formData: Roomate) => {
    if (roomateId) {
      dispatch(updateRoomate({ ...formData, id: roomateId }));
    }
    else dispatch(addRoomate(formData))
    redirectToRoomatesView();
  }

  const cancelForm = () => {
    redirectToRoomatesView();
  }

  const redirectToRoomatesView = () => {
    props.history.push("/roomates");
  }

  const formProps: IFormProps<Roomate> = {
    schema: {
      name: {
        type: InputFieldType.Text,
        label: roomatesResources.forms.name.label,
        placeholder: roomatesResources.forms.name.placeholder,
        required: true,
        value: roomate?.name || "",
        valid: false
      }
    },
    saveFormData,
    cancelForm
  }
  return (
    <div className="section">
      <SectionHeader title={formName} loading={false}/>
      <div className="section-content form">
        <Form {...formProps}/>
      </div>
    </div>
  )
}

export default withRouter(RoomateForm);