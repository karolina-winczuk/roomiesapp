import React, { useEffect } from "react"
import { useDispatch } from "react-redux"
import { Redirect } from "react-router-dom"
import { logOut } from "../../Store/actions/AuthActions"

const LogOut: React.FC<{}> = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(logOut());
  })
  return <Redirect to="/log-in" />
}

export default LogOut;