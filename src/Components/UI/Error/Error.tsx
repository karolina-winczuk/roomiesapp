interface IErrorProps {
    message: string;
}

const error: React.FC<IErrorProps> = ({ message }) => (
    message ? <div className="error">{message}</div> : null
);

export default error;