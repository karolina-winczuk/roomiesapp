import { render, screen } from "@testing-library/react";
import Error from "./Error";
describe("Error component", () => {
    it("renders message", () => {
        render(<Error message="Some error message"/>);
        expect(screen.getByText("Some error message")).toBeInTheDocument();
    })

    it("not renders when message is empty", () => {
        const { container } = render(<Error message=""/>);
        expect(container.firstChild).toBeNull();
    })
})