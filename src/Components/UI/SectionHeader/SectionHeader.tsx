import React from "react";
import Loader from "../Loader/Loader";

interface ISectionHeaderProps {
    title: string;
    loading: boolean
}
const sectionHeader: React.FC<ISectionHeaderProps> = ({ title, loading}) => (
    <h2 className="section-header">
        <span>{title}</span>
        <span><Loader show={loading} /></span>
    </h2>
);

export default sectionHeader;