export const Backdrop: React.FC<{ onClick: () => void }> = ({ onClick }) => {
    return <div className="backdrop" onClick={onClick}></div>
}