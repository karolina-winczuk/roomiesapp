import { IShow } from "../../../Interfaces/IShow";

const loader: React.FC<IShow> = ({ show }) => (
    show ? <div className="loader"></div> : null
)

export default loader;