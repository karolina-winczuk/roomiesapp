import { render, screen } from "@testing-library/react";
import Loader from "./Loader";
describe("Loader component", () => {
    it("renders", () => {
        const { container } = render(<Loader show={true}/>);
        expect(container.firstChild).not.toBeNull();
    })

    it("not renders", () => {
        const { container } = render(<Loader show={false} />);
        expect(container.firstChild).toBeNull();
    })
})