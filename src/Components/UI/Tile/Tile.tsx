import React from "react";

const tile: React.FC<{ title: string, icon: string }> = ({ title, icon }) => (
    <React.Fragment>
        <span>{title}</span>
        <i className={icon}></i>
    </React.Fragment>
);

export default tile;