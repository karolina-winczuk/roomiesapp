import { IColumnDescription } from "../../../Interfaces/IColumnDescription";
import { TableRow } from "./TableRow";

export interface ITableBodyProps<T> {
	dataItems: Array<T>;
	schema: Array<IColumnDescription>;
}

export const TableBody = <T extends { id: string },>({ dataItems, schema }: ITableBodyProps<T>) => {
	const rows = dataItems.map(item => <TableRow dataItem={item} schema={schema} />)
	return (
		<tbody>
			{rows}
		</tbody>
	)
}