import { IColumnDescription } from "../../../Interfaces/IColumnDescription";

export interface ITableRowProps<T extends { id: string }> {
	dataItem: T,
	schema:  Array<IColumnDescription>
}

export const TableRow = <T extends { id: string },>({ dataItem, schema }: ITableRowProps<T>) => {
	const cells = schema.map(field => <td>{field.template?.(dataItem) || dataItem[field.fieldName as keyof T]}</td>)
	return (
		<tr key={dataItem.id}>
			{cells}
		</tr>
	)
}