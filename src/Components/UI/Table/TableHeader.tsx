import { IColumnDescription } from "../../../Interfaces/IColumnDescription";

export const TableHeader: React.FC<{ schema: Array<IColumnDescription> }> = ({ schema }) => {
	const headers = schema.map(field => <th>{field.header || field.fieldName}</th>);
	return (
		<thead>
			<tr>
				{headers}
			</tr>
		</thead>
	)
}