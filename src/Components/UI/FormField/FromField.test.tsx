import FormField from "./FormField";
import { IFormFieldProps } from "../../../Interfaces/IFormFieldProps";
import { FormFieldType } from "../../../Models/FormFieldType";
import { render, screen } from "@testing-library/react";

describe("FormField component", () => {
  it("renders text field type", () => {
    const props: IFormFieldProps = {
      fieldName: "fieldName",
      fieldSchema: {
        type: FormFieldType.Text,
        value: "x",
        label: "Field Name",
        required: true,
        valid: false
      },
      handleChange: jest.fn
    }
    render(<FormField {...props} />)
    expect(screen.getByRole('textbox')).toHaveAttribute("type", "text");
  })
})