export interface IClearSelectionProps {
    visible: boolean; 
    onClear: () => void;
}
export const ClearSelection: React.FC<IClearSelectionProps> = ({ visible, onClear }) => (
    visible ? <span onClick={onClear}>X</span> : null
)