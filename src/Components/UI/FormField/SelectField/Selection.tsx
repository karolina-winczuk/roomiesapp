import React from "react";
import { ISelectOption } from "../../../../Models/ISelectOption"
import { resources } from "../../../../Resources/Resources";
import { Placeholder } from "./Placeholder";

export interface ISelectionProps {
    selectedOptions: Array<ISelectOption>;
}

const selectResources = resources.forms.select;

export const Selection: React.FC<ISelectionProps> = ({ selectedOptions }) => {
    const selections = selectedOptions.map(option => (
        <div className="selected-option" key={option.key}>
            {option.value}
        </div>)
    )
    return (
        <div className="selected-options">{selections?.length > 0 ? selections : <Placeholder message={selectResources.empty} />}</div> 
    )
}