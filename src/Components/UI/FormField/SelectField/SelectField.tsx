import React, { useEffect, useState } from "react";
import { IFormFieldProps } from "../../../../Interfaces/IFormFieldProps";
import { ISelectOption } from "../../../../Models/ISelectOption";
import { SelectFieldType } from "../../../../Models/SelectFieldType";
import { ClearSelection } from "./ClearSelection";
import { Selection } from "./Selection";
import { SelectOptions } from "./SelectOptions";

export const SelectField: React.FC<IFormFieldProps> = props => {
    const isMulti = props.fieldSchema.type === SelectFieldType.Multi;
	const initialValue = props.fieldSchema.value;
	let defaultValue: Array<ISelectOption> = [];
	if (isMulti)
		defaultValue = initialValue && Array.isArray(initialValue) ? initialValue : defaultValue;
	else defaultValue = initialValue ? [initialValue] : defaultValue
    const [values, setValues] = useState<Array<ISelectOption>>(defaultValue);
    const [optionsOpened, setOptionsOpened] = useState(false);
    
    useEffect(() => {
        props.handleChange(isMulti ? values : values[0], props.fieldName)
    }, [values]);

    const onSelect = (selectedOption: ISelectOption) => {
        if (isMulti) {
            const selected = values.some(val => val.key === selectedOption.key);
            if (selected) setValues(values.filter(val => val.key !== selectedOption.key))
            else setValues([...values, selectedOption])
        }
        else setValues([selectedOption]);
    }

    const clearSelections = () => {
        setValues([]);
    }

    const toggleOptions = () => {
        setOptionsOpened(!optionsOpened)
    }

    const selectTypeClass = isMulti ? "select-input multi" : "select-input";
    const selectInputClass = optionsOpened ? `${selectTypeClass} focus` : selectTypeClass;
    const showClearSelection = isMulti && values.length > 0;
    return (
        <div className="select" onFocus={() => toggleOptions()} onBlur={() => toggleOptions()} tabIndex={0}>
            <div className={selectInputClass} >
                <Selection selectedOptions={values}/>
                <ClearSelection visible={showClearSelection} onClear={clearSelections} />
            </div>
            <SelectOptions options={props.fieldSchema.options} selectedOptions={values} onSelect={onSelect} isVisible={optionsOpened} />
        </div>
    )
}