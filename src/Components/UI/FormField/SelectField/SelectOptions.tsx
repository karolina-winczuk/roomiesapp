import React from "react";
import { ISelectOption } from "../../../../Models/ISelectOption";
import { resources } from "../../../../Resources/Resources";
import { Placeholder } from "./Placeholder";

export interface ISelectOptionsProps {
    options: Array<ISelectOption>;
    selectedOptions: Array<ISelectOption>;
    onSelect: (option: ISelectOption) => void;
    isVisible: boolean
}

const selectResources = resources.forms.select;
export const SelectOptions: React.FC<ISelectOptionsProps> = ({ options, selectedOptions, onSelect, isVisible }) => {
    let optionElements = options.map((option: ISelectOption) => (
        <li key={option.key} className={selectedOptions.some(val => val.key === option.key) ? "option selected" : "option"} onClick={() => onSelect(option)}>
            {option.value}
        </li>)
    );
    const selectOptionsClass = isVisible ? "select-options" : "select-options hidden"
    return (
        <ul className={selectOptionsClass}>
            {optionElements.length ? optionElements : <Placeholder message={selectResources.noItems} />}
        </ul>
    )
}