import { IFormFieldProps } from "../../../Interfaces/IFormFieldProps"

export const InputField: React.FC<IFormFieldProps> = ({ fieldSchema, handleChange, fieldName }) => {
    const fieldProps = {
        value: fieldSchema.value,
        onChange: (e: React.ChangeEvent<HTMLInputElement>) => handleChange(e.target.value, fieldName),
        placeholder: fieldSchema.placeholder || "",
        required: fieldSchema.required
      }
    return (
        <input type={fieldSchema.type} {...fieldProps} />
    )
}