import React from 'react';
import { IFormFieldProps } from '../../../Interfaces/IFormFieldProps';
import { InputFieldType } from '../../../Models/InputFieldType';
import { SelectFieldType } from '../../../Models/SelectFieldType';
import { InputField } from './InputField';
import { SelectField } from './SelectField/SelectField';

export const formField: React.FC<IFormFieldProps> = props => {
  let field;
  switch(props.fieldSchema.type) {
    case SelectFieldType.Single:
    case SelectFieldType.Multi:
      field = <SelectField {...props} />
      break;
    case InputFieldType.Password:
    case InputFieldType.Email:
    default:
      field = <InputField {...props} />;
      break;
  }
  
  return (
    <React.Fragment>
      <div className="field-label">
        <label>{props.fieldSchema.label}</label>
      </div>
      <div className="field-input">{field}</div>
    </React.Fragment>
  )
}

export default formField;