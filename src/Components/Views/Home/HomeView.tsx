import { resources } from "../../../Resources/Resources";

const homeView: React.FC<any> = props => {
  const homeResources = resources.home;
  return (
      <div className="home-view">
        <div className="home-content">
          <h1 className="title">{homeResources.header}</h1>
          <p className="description">{homeResources.description}</p>
        </div>
      </div>
  )
};

export default homeView;