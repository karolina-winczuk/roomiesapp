import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IStoreState } from "../../../Interfaces/IStoreState";
import { IExpence } from "../../../Models/IExpence";
import { resources } from "../../../Resources/Resources"
import { getExpences } from "../../../Store/actions/ExpencesActions";
import SectionHeader from "../../UI/SectionHeader/SectionHeader";
import { ExpencesList } from "./ExpencesList";
import { ExpencesMenu } from "./ExpencesMenu";

const expencesResources = resources.expences;

export const ExpencesView: React.FC<{}> = () => {
    const loading = false;
    const expences = useSelector<IStoreState, Array<IExpence>>(state => state.expences.data);
	const dispatch = useDispatch();

    useEffect(() => {
		if (!(expences.length > 0))
			dispatch(getExpences());
    }, [])
    return (
        <div className="view-content expences">
            <SectionHeader title={expencesResources.name} loading={loading} />
            <div className="section-content">
                <ExpencesMenu />
				<ExpencesList expences={expences} />
            </div>
        </div>
    )
}