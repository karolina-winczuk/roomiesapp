import React from "react";
import { Link } from "react-router-dom";
import { resources } from "../../../Resources/Resources";
import Tile from "../../UI/Tile/Tile";

const expencesResources = resources.expences;
export const ExpencesMenu: React.FC<{}> = () => {
	return (
        <ul className="menu-tiles">
			<li>
				<Link className="menu-tile" to={"/expences/expence-form"}>
					<Tile title={expencesResources.addExpence} icon={"bi bi-plus-square"} />
				</Link>
			</li>
		</ul>
    )
}