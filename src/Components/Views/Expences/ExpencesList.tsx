import React from "react";
import { IColumnDescription } from "../../../Interfaces/IColumnDescription";
import { IExpence } from "../../../Models/IExpence"
import { resources } from "../../../Resources/Resources"
import { Table } from "../../UI/Table/Table";
import { TableBody } from "../../UI/Table/TableBody";
import { TableHeader } from "../../UI/Table/TableHeader";

const expencesResources = resources.expences.forms;
export const ExpencesList: React.FC<{ expences: Array<IExpence>}> = ({ expences }) => {
	const schema: Array<IColumnDescription> = [
		{ 
			fieldName: "amount", 
			template: (expence: IExpence) => <div>{`${expence.amount} ${expence.currency?.symbol}` }</div>,
			header: expencesResources.amount.label 
		},
		{ 
			fieldName: "paidBy", 
			template: (expence: IExpence) => <div>{expence.paidBy.name}</div>, 
			header: expencesResources.paidBy.label 
		}, 
		{ 
			fieldName: "paidFor", 
			template: (expence: IExpence) => (
				<div className="paidFor">
					{expence.paidFor.map(p => <span title={p.name?.slice(1)}>{p.name?.charAt(0)}</span>)}
				</div>
			), 
			header: expencesResources.paidFor.label 
		}
	]
	return (
		<Table>
			<TableHeader schema={schema} />
			<TableBody schema={schema} dataItems={expences} />
		</Table>
	)
}
