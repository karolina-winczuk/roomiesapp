import React from "react";
import { Roomate } from "../../../Models/Roomate";
import RoomatesListItem from "./RoomatesListItem";

interface IRoomatesListProps {
    selected?: Roomate;
    roomates: Array<Roomate>;
    handleClick: (e: React.MouseEvent<HTMLLIElement>, roomate: Roomate) => void
}

const roomatesList: React.FC<IRoomatesListProps> = ({ roomates, handleClick, selected }) => (
    <ul className="roomates-list">
        {roomates?.map((roomate: Roomate) => (
            <RoomatesListItem key={roomate.id} selected={selected?.id === roomate.id} roomate={roomate} handleClick={handleClick} />
        ))}
    </ul>
);

export default roomatesList;