import React from "react";
import { Roomate } from "../../../Models/Roomate";

interface IRoomatesListItemProps {
    selected: boolean;
    roomate: Roomate,
    handleClick: (e: React.MouseEvent<HTMLLIElement>, roomate: Roomate) => void
}

const roomatesListItem: React.FC<IRoomatesListItemProps> = ({ roomate, handleClick, selected }) => (
    <React.Fragment>
        <li className={selected ? "selected" : ""} key={roomate.id} onClick={(e) => handleClick(e, roomate)}>
            <span className="avatar"></span>
            <span>{roomate.name}</span>
        </li>
    </React.Fragment>
)

export default roomatesListItem;