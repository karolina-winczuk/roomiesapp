import React from "react";
import { Link } from "react-router-dom";
import { Roomate } from "../../../Models/Roomate";
import { resources } from "../../../Resources/Resources";
import Tile from "../../UI/Tile/Tile";

interface IRoomatesMenu {
    selectedRoomate: Roomate,
    onDeleteRoomate: Function
}

const roomatesMenu: React.FC<IRoomatesMenu> = ({selectedRoomate, onDeleteRoomate}) => {
    const menuButtonClass = selectedRoomate ? "menu-tile" : "menu-tile disabled";
    const roomatesResources = resources.roomates;
    return (
        <ul className="menu-tiles">
            <li>
                <Link className="menu-tile" to={"/roomates/roomate-form"}>
                    <Tile title={roomatesResources.addRoomate} icon={"bi bi-plus-square"} />
                </Link>
            </li>
            <li><Link className={menuButtonClass} to={selectedRoomate?.id ? `/roomates/roomate-form?id=${selectedRoomate?.id}` : '/roomates/roomate-form'}>
                    <Tile title={roomatesResources.editRoomate} icon={"bi-pencil-square"} />
                </Link>
            </li>
            <li><div className={menuButtonClass} onClick={(e) => onDeleteRoomate(selectedRoomate)}>
                    <Tile title={roomatesResources.deleteRoomate} icon={"bi bi-x-square"} />
                </div>
            </li>
        </ul>
    )
}

export default roomatesMenu;