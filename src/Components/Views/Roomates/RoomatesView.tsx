import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Roomate } from "../../../Models/Roomate";
import { IStoreState } from "../../../Interfaces/IStoreState";
import { deleteRoomate, getRoomates } from "../../../Store/actions/RoomatesActions";
import RoomatesList from "./RoomatesList";
import RoomatesMenu from "./RoomatesMenu";
import Error from "../../UI/Error/Error";
import { resources } from "../../../Resources/Resources";
import SectionHeader from "../../UI/SectionHeader/SectionHeader";

const RoomatesView: React.FC<{}> = () => {
  const roomates = useSelector<IStoreState, Array<Roomate>>(state => state.roomates.data);
  const error = useSelector<IStoreState, string>(state => state.roomates.error);
  const loading = useSelector<IStoreState, boolean>(state => state.roomates.loading);
  const dispatch = useDispatch();
  const [selectedRoomate, setSelectedRoomate] = useState<Roomate | undefined>(undefined);

  useEffect(() => {
    if (!roomates?.length) {
      dispatch(getRoomates());
    }
  },[])

  const selectionHandler = (event: React.MouseEvent<HTMLLIElement>, roomate: Roomate) => {
    setSelectedRoomate(roomate);
  }

  const deleteHandler = (event: React.MouseEvent<HTMLLIElement>) => {
    dispatch(deleteRoomate(selectedRoomate!));
    setSelectedRoomate(undefined);
  }

  return (
    <div className="view-content">
      <SectionHeader title={resources.roomates.name} loading={loading} />
      <div className="section-content">
        <RoomatesMenu selectedRoomate={selectedRoomate!} onDeleteRoomate={deleteHandler}/>
        <Error message={error} />
        <RoomatesList selected={selectedRoomate} roomates={roomates} handleClick={selectionHandler}/>
      </div>
    </div>
  )
}

export default RoomatesView;