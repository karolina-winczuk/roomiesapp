import { FormFieldType } from "../Models/FormFieldType";

export interface IFormFieldSchema {
  value: any;
  label: string;
  placeholder?: string;
  required: boolean;
  valid: boolean;
  [property: string]: any;
}