import { ISelectOption } from "../Models/ISelectOption";
import { IInputFieldSchema } from "./IInputFieldSchema";
import { ISelectFieldSchema } from "./ISelectFieldSchema";

export interface IFormFieldProps {
  fieldName: string;
  fieldSchema: IInputFieldSchema | ISelectFieldSchema;
  handleChange: (value: string | number | ISelectOption | Array<ISelectOption>, fieldName: string) => void;
}