import { IExpence } from "../Models/IExpence";
import { Roomate } from "../Models/Roomate";

export interface IStoreState {
  roomates: IRoomatesState;
  expences: IExpencesState;
  auth: IAuthState;
}

export type IRoomatesState = IDataReducerState<Roomate>;
export type IExpencesState = IDataReducerState<IExpence>;

export interface IDataReducerState<T> extends ICurrentState {
  data: Array<T>;
}

export interface IAuthState extends ICurrentState {
  isAuth: boolean,
  tokenId: string,
  tokenExpires: number,
  houseId: string
}

interface ICurrentState {
  loading: boolean;
  error: string;
}