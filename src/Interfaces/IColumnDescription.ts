export interface IColumnDescription {
	fieldName: string;
	template?: (value: any) => JSX.Element | string;
	header?: string;
}