import { InputFieldType } from "../Models/InputFieldType";
import { IFormFieldSchema } from "./IFormFieldSchema";

export interface IInputFieldSchema extends IFormFieldSchema {
    type: InputFieldType;
}