import { ISelectOption } from "../Models/ISelectOption";
import { SelectFieldType } from "../Models/SelectFieldType";
import { IFormFieldSchema } from "./IFormFieldSchema";

export interface ISelectFieldSchema extends IFormFieldSchema {
    type: SelectFieldType;
    options: Array<ISelectOption>;
}