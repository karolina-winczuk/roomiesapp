export const config = {
  databaseUrl: "https://roomiesappdb-default-rtdb.firebaseio.com/",
  authBaseUrl: "https://identitytoolkit.googleapis.com/v1/accounts",
  apiKey: process.env.REACT_APP_API_KEY
}