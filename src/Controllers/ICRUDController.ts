export interface ICRUDController<T> {
	create(item: T, tokenId: string, houseId: string): Promise<T>;
	read(tokenId: string, houseId: string): Promise<Array<T>>;
	update(item: T, tokenId: string, houseId: string): Promise<T>;
	delete(itemId: string, tokeId: string): Promise<void>;
}