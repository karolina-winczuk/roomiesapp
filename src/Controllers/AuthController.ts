import axios, { AxiosResponse } from "axios";
import { config } from "../Config/config";
import { AuthData } from "../Models/AuthData";
import { AuthCredential } from "../Models/AuthCredential";

export class AuthController {
	signUp(data: AuthCredential): Promise<AuthData> {
		return axios.post(`${config.authBaseUrl}:signUp?key=${config.apiKey}`, {
			email: data.login,
			password: data.password,
			returnSecureToken: true
		}).then(response => this._processAuthResponse(response))
	}

	logIn(data: AuthCredential): Promise<AuthData> {
		return axios.post(`${config.authBaseUrl}:signInWithPassword?key=${config.apiKey}`, {
			email: data.login,
			password: data.password,
			returnSecureToken: true
		}).then(response => this._processAuthResponse(response))
	}

	logOut(): AuthData {
		AuthController.clearCachedData();
		return new AuthData();
	}

	private _processAuthResponse(response: AxiosResponse<{[string: string]: string}>): AuthData {
		const data = response.data;
		const authData = new AuthData(data.idToken, +data.expiresIn, data.localId);
		AuthController.persistAuthData(authData);
		return authData;
	}

	public static persistAuthData(authData: AuthData) {
		localStorage.setItem("authTokenId", authData.tokenId);
		localStorage.setItem("authLocalId", authData.houseId);
		localStorage.setItem("authTokenExpires", AuthController._getExpirationTime(authData.tokenExpires));
	}

	public static getCachedData(): AuthData | null {
		const tokenId = localStorage.getItem("authTokenId");
		const houseId = localStorage.getItem("authLocalId");
		const tokenExpires = localStorage.getItem("authTokenExpires");
		if (tokenId && houseId && tokenExpires && AuthController._checkIfTokenValid(+tokenExpires)) {
			return new AuthData(tokenId, +tokenExpires, houseId);
		}
		AuthController.clearCachedData();
		return null;
	}

	public static clearCachedData() {
		localStorage.removeItem("authTokenId");
		localStorage.removeItem("authLocalId");
		localStorage.removeItem("authTokenExpires");
	}

	private static _checkIfTokenValid(expirationTime: number) {
		return expirationTime > new Date().getTime();
	}

	private static _getExpirationTime(expiresIn: number) {
		return (new Date().getTime() + expiresIn*1000).toString();
	}
}