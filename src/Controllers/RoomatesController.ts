import { Roomate } from "../Models/Roomate";
import { GenericCRUDController } from "./GenericCRUDController";

export class RoomatesController extends GenericCRUDController<Roomate> {
  readonly endpoint = "roomates";
}