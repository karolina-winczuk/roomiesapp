import axios from "axios";
import { config } from "../Config/config";
import { ICRUDController } from "./ICRUDController";

export abstract class GenericCRUDController<T extends { id: string }> implements ICRUDController<T> {
	abstract readonly endpoint: string;

	create(item: T, tokenId: string, houseId: string): Promise<T> {
		return axios.post(config.databaseUrl + `${this.endpoint}.json?auth=${tokenId}`, { ...item, houseId: houseId }).then(response => {
			return { ...item, id: response.data.name };
		})
	}

	read(tokenId: string, houseId: string): Promise<Array<T>> {
		return axios.get(config.databaseUrl + `${this.endpoint}.json?auth=${tokenId}&orderBy="houseId"&equalTo="${houseId}"`)
			.then(response => {
				const data = [] as Array<T>;
				if (response.data) {
					Object.keys(response.data)?.forEach(key => {
					data.push({ ...response.data[key], id: key })
					})
				}
				return data;
			})
	}

	update(item: T, tokenId: string, houseId: string): Promise<T> {
		return axios.put(config.databaseUrl + `${this.endpoint}/${item.id}.json?auth=${tokenId}`, { ...item, houseId: houseId })
			.then(response => response.data);
	}

	async delete(itemId: string, tokenId: string): Promise<void> {
		await axios.delete(config.databaseUrl + `${this.endpoint}/${itemId}.json?auth=${tokenId}`)
	}
}