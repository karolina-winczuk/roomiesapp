import { IExpence } from "../Models/IExpence";
import { GenericCRUDController } from "./GenericCRUDController";

export class ExpencesController extends GenericCRUDController<IExpence> {
  endpoint = "expences";
}