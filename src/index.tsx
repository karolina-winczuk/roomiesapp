import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, combineReducers, CombinedState } from 'redux';
import { reducer as roomates } from './Store/reducers/RoomatesReducer';
import { reducer as auth } from './Store/reducers/AuthReducer';
import { reducer as expences } from './Store/reducers/ExpencesReducer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { IStoreState } from './Interfaces/IStoreState';
import { IAction } from './Interfaces/IAction';
import App from './App';
import { AuthData } from './Models/AuthData';

const appReducer = combineReducers<IStoreState>({ auth, roomates, expences });

const rootReducer = (state: CombinedState<IStoreState> | undefined, action: IAction<AuthData>) => {
  if (action.type === 'LOG_OUT') {
    state = undefined;
  }
  return appReducer(state, action);
}

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}><App /></Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
