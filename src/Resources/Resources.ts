export const resources = {
    logo: "RoomiesApp",
    buttons: {
        submit: "Submit",
        cancel: "Cancel"
    },
    auth: {
        login: "Log in",
        signup: "Sign up",
        logout: "Log out",
        form: {
            login: {
                label: "Login",
                placeholder: "Enter house login",
            },
            password: {
                label: "Password",
                placeholder: "Enter password"
            }
        }
    },
    roomates: {
        name: "Roomates",
        addRoomate: "Add roomate",
        editRoomate: "Edit roomate",
        deleteRoomate: "Delete roomate",
        forms: {
            name: {
                label: "Name",
                placeholder: "Enter roomate name"
            }
        }
    },
    expences: {
        name: "Expences",
        addExpence: "Add expence",
        forms: {
            amount: {
                label: "Amount"
            },
			currency: {
				label: "Currency"
			},
            paidBy: {
                label: "Paid by"
            },
            paidFor: {
                label: "Paid for"
            }
        }
    },
    chores: {
        name: "Chores"
    },
    home: {
        header: "Welcome to RoomiesApp!",
        description: "Plan chores and split house expences with your flatmates or roomates"
    },
    messages: {
        error: "Something went wrong!"
    },
    forms: {
        select: {
            empty: "Select...",
			noItems: "No items available"
        }
    }
}