import { IInputFieldSchema } from "../Interfaces/IInputFieldSchema";
import { ISelectFieldSchema } from "../Interfaces/ISelectFieldSchema";

export type FormFieldSchema = IInputFieldSchema | ISelectFieldSchema;