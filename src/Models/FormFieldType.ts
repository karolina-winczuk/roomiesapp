import { InputFieldType } from "./InputFieldType"
import { SelectFieldType } from "./SelectFieldType"

export type FormFieldType = InputFieldType | SelectFieldType;