export interface Roomate {
    id: string;
    name: string;
}