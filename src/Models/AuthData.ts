export class AuthData {
  tokenId: string;
  tokenExpires: number;
  houseId: string;

  constructor(tokenId?: string, tokenExpires?: number, houseId?: string) {
    this.tokenId = tokenId || "";
    this.tokenExpires = tokenExpires || 0;
    this.houseId = houseId || "";
  }
}