export enum InputFieldType {
    Text = "text",
    Number = "number",
    Password = "password",
    Email = "email"
}