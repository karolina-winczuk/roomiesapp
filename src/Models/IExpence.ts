import { Currency } from "./Currency";
import { Roomate } from "./Roomate";

export interface IExpence {
    id: string;
    amount: number;
	currency: Currency;
    paidBy: Roomate;
    paidFor: Array<Roomate>;
}