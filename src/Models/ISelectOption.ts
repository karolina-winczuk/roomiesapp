export interface ISelectOption { 
    value: string;
    key: string; 
}