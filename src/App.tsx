import React from 'react';
import { BrowserRouter } from "react-router-dom";
import { Navigation } from './Components/Navigation/Navigation';
import { RoutesSwitcher } from './Components/RoutesSwitcher';
import "./Styles/styles.scss";

const App: React.FC<{}> = () => {
  return (
    <div className="app">
      <BrowserRouter>
        <Navigation />
        <div className="content">
          <RoutesSwitcher />
        </div>
      </BrowserRouter>
    </div>
  )
}

export default App;
